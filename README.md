Hi there!

You can use script in 2 ways:
- python dg_xml_app/main.py
- pip install dg_xml_app....
    and after use shortcut xml_parser

args:
-file_path %PATH% (required) - file path to XML file

OPTIONS:

--interval YYYY-MM-DD:YYYY-MM-DD - interval dates

--username "user.a, user.b ..." - set Users to check

Docker usage:

docker run -it --rm -v "$PWD":/tmp/xml_parse/ docker_img -file_path {path} {OPTIONS}
