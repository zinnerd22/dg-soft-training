from lxml import etree


def parse_item(in_file: str) -> tuple:
    """ Генератор парсер xml"""
    for event, element in etree.iterparse(in_file, tag="person"):
        username = element.attrib['full_name']
        dates = [child.text for child in element]
        element.clear()
        yield dates, username
