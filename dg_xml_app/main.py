from dg_xml_app.args import get_parameters
from dg_xml_app.xml_runtime import parse_item
from dg_xml_app.runtime import Worker


def print_out(new_worker):
    for key, date_dict in new_worker.store.items():
        print(key, ' : ')
        for date, delta in date_dict.store.items():
            print(date, ' : ', delta)


def main(cmd_args=None):
    file_path, date_interval, username = get_parameters(cmd_args)
    new_worker = Worker(date_interval=date_interval, usernames_list=username)
    for item in parse_item(file_path):
        new_worker.append(dates=item[0], username=item[1])
    return new_worker


def main_script(cmd_args=None):
    parsed_info = main(cmd_args)
    print_out(parsed_info)


if __name__ == '__main__':
    main_script()

