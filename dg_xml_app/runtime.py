from datetime import datetime
import datetime as dt

DATE_CFG = '%Y-%m-%d'
DATE_TIME_CFG = '%d-%m-%Y %H:%M:%S'


def str_to_date(str_date: str) -> dt.date:
    """конвертируем строку в дату"""
    return datetime.strptime(str_date, DATE_CFG).date()


def str_to_date_time(str_dt: str) -> dt.datetime:
    """конвертируем строку в дата-время"""
    return datetime.strptime(str_dt, DATE_TIME_CFG)


class TimeKeeper:
    """класс - хранилище, содержит в качестве ключа - дату, в качестве значение time-delta"""
    def __init__(self, date_intervals: tuple = None):
        self.store = dict()
        self.chk_date_intervals = False
        if date_intervals:
            self.chk_date_intervals = True
            self.date_in = date_intervals[0]
            self.date_out = date_intervals[1]

    def check_in_date(self, check_date: dt.date) -> bool:
        """Метод проверяет входит ли дата в необходимый нам интервал"""
        if self.chk_date_intervals is False or (self.date_in <= check_date) and (check_date <= self.date_out):
            return True
        else:
            return False

    def append(self, date_time_in: str, date_time_out: str) -> None:
        """Метод добавления. Принимает две даты, и сохраняет разницу"""
        date_check = str_to_date_time(date_time_in).date()
        if self.check_in_date(date_check):
            time_delta = str_to_date_time(date_time_out) - str_to_date_time(date_time_in)
            try:
                self.store[date_check] += time_delta
            except KeyError:
                self.store[date_check] = time_delta


class Worker:
    """Класс, помогающий обработать XML. Принимает интервал дат для обработки (опция)
    а так же список пользователей через запятую, так же опция"""
    def __init__(self, date_interval: tuple = None, usernames_list: str = 'all'):
        self.date_interval = date_interval
        self.usernames_list = usernames_list
        self.store = dict()

    def append(self, dates: list, username: str = 'all') -> None:
        """метод добавления xml данных для расчета"""
        if self.usernames_list == 'all' or username in self.usernames_list:
            store_key = self.get_store_key(username)
            try:
                self.store[store_key].append(dates[0], dates[1])
            except KeyError:
                self.store[store_key] = TimeKeeper(self.date_interval)
                self.store[store_key].append(dates[0], dates[1])

    def get_store_key(self, u_name: str) -> str:
        if self.usernames_list == 'all':
            return 'all'
        else:
            return u_name
