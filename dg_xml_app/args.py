from argparse import ArgumentParser, Namespace
from dg_xml_app.runtime import str_to_date
import os.path


def parser(cmd_args: str) -> Namespace:
    """Организуем обработку аргументов"""
    arg_parser = ArgumentParser(description='Here is some info for my home Script: ')
    arg_parser.add_argument('-file_path', type=str, help='select there path to xml file')
    arg_parser.add_argument('--interval', type=str, help='set there date interval to limit results')
    arg_parser.add_argument('--username', type=str, help='set there username to limit results')
    return arg_parser.parse_args(cmd_args)


def check_file_path(file_path: str) -> bool:
    """проверяем существует ли файл переданный нам как параметр"""
    try:
        is_file = os.path.isfile(file_path)
        if is_file:
            return True
    except (IOError, TypeError):
        raise IOError('selected path is not a file')


def check_option_parameters(time_interval: str) -> tuple:
    """Проверяем что в опции интервал даты заданы верно"""
    time_1, time_2 = time_interval.strip().split(":")
    try:
        date_1 = str_to_date(time_1)
        date_2 = str_to_date(time_2)
    except ValueError:
        raise ValueError('Time convert error there, Stoping')
    return date_1, date_2


def get_parameters(cmd_args=None) -> tuple:
    """обрабатываем полуенные параметры"""
    parameters = parser(cmd_args)
    check_file_path(parameters.file_path)
    date_intervals = None
    username_list = 'all'
    if parameters.interval:
        date_intervals = check_option_parameters(parameters.interval)
    if parameters.username:
        username_list = parameters.username.strip().split(', ')
    return parameters.file_path, date_intervals, username_list
