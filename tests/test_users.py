import pytest

from dg_xml_app.main import main
from datetime import timedelta
from dg_xml_app.runtime import str_to_date

CMD_ARGS_1 = ['-file_path', 'dg_xml_app\\test.xml', '--username', 'a.stepanova']


def test_user_case_worker():
    worker = main(CMD_ARGS_1)
    assert ['a.stepanova'] == list(worker.store.keys())


def test_user_case_t_keeper_keys():
    worker = main(CMD_ARGS_1)
    w_tk = worker.store['a.stepanova']
    assert [str_to_date('2011-12-21')] == list(w_tk.store.keys())


def test_user_case_t_keeper_val():
    worker = main(CMD_ARGS_1)
    w_tk = worker.store['a.stepanova']
    w_time = w_tk.store[str_to_date('2011-12-21')]
    assert w_time == timedelta(seconds=29945)
