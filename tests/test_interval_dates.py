import pytest

from dg_xml_app.main import main
from dg_xml_app.runtime import str_to_date
from datetime import timedelta

CMD_ARGS_1 = ['-file_path', 'dg_xml_app\\test.xml', '--username', 'i.ivanov']
CMD_ARGS_2 = ['-file_path', 'dg_xml_app\\test.xml', '--username', 'i.ivanov', '--interval', '2011-12-22:2011-12-23']
CMD_ARGS_3 = ['-file_path', 'dg_xml_app\\test.xml', '--interval', '2011-12-20:2011-12-23']


def test_dates_case():
    worker = main(CMD_ARGS_1)
    w_tk = worker.store['i.ivanov']
    assert len(w_tk.store.keys()) == 3


def test_dates_case_2():
    worker = main(CMD_ARGS_2)
    w_tk = worker.store['i.ivanov']
    assert len(w_tk.store.keys()) == 1


def test_dates_case_3():
    worker = main(CMD_ARGS_3)
    w_tk = worker.store['all']
    total_time = w_tk.store[str_to_date('2011-12-21')]
    assert total_time == timedelta(seconds=61640)
